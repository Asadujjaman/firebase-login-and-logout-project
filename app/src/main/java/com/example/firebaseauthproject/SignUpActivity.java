package com.example.firebaseauthproject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;

import java.util.regex.Pattern;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener{
    private TextView textView_signIn;
    private EditText edtTextEmail,edtTextpassword;
    ProgressBar progressBar;

    private FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        edtTextEmail = findViewById(R.id.user_email);
        edtTextpassword = findViewById(R.id.user_password);

        progressBar = findViewById(R.id.progress);

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        //another way to setup onClick Listener;
        findViewById(R.id.btn_signUp).setOnClickListener(this);
        findViewById(R.id.sign_in_text_view).setOnClickListener(this);

    }

    private void registerUser(){
        String email = edtTextEmail.getText().toString().trim();
        String password = edtTextpassword.getText().toString().trim();
        if (email.isEmpty()){
            edtTextEmail.setError("Email is required");
            edtTextEmail.requestFocus();
            return;
        }

        //check valid email address then type like "android.util.Patterns.EMAIL_ADDRESS;"
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            edtTextEmail.setError("Please Enter a valid Email ");
            edtTextEmail.requestFocus();
            return;

        }
        if (password.isEmpty()){
            edtTextpassword.setError("Password is required");
            edtTextpassword.requestFocus();
            return;
        }
        if (password.length()<6){
            edtTextpassword.setError("Minimum password length should be 6 ");
            edtTextpassword.requestFocus();
            return;
        }
        progressBar.setVisibility(View.VISIBLE);

        mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                progressBar.setVisibility(View.GONE);
                if (task.isSuccessful()){
                    finish();
                   Intent intent = new Intent(SignUpActivity.this,ProfileActivity.class);
                   startActivity(intent);
                }else {
                    if (task.getException() instanceof FirebaseAuthUserCollisionException){
                        Toast.makeText(SignUpActivity.this, "You are already Registered ", Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(SignUpActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_signUp:
                registerUser();
                break;
            case R.id.sign_in_text_view:
                finish();
                Intent intent = new Intent(SignUpActivity.this,MainActivity.class);
                startActivity(intent);
                break;
        }
    }
}
