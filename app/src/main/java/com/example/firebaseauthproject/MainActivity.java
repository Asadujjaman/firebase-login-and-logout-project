package com.example.firebaseauthproject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
   // private TextView textView_signup;
    ProgressBar progressBar;
    EditText edtTextemail,edtTextpassword;
        FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAuth = FirebaseAuth.getInstance();
        edtTextemail = findViewById(R.id.user_email);
        edtTextpassword=findViewById(R.id.user_password);
        progressBar = findViewById(R.id.progress_id);

        //alternative onclick listener.
       findViewById(R.id.sign_up_text_view).setOnClickListener(this);
       findViewById(R.id.btn_login).setOnClickListener(this);


    }

    private void userLogin(){
        String email = edtTextemail.getText().toString().trim();
        String password =edtTextpassword.getText().toString().trim();
        if (email.isEmpty()){
            edtTextemail.setError("Email is required");
            edtTextemail.requestFocus();
            return;
        }
        if (password.isEmpty()){
            edtTextpassword.setError("Password is required");
            edtTextpassword.requestFocus();
            return;
        }
        if (password.length()<6){
            edtTextpassword.setError("Minimum password should be 6 ");
            edtTextpassword.requestFocus();
            return;
        }
        progressBar.setVisibility(View.VISIBLE);
        mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                progressBar.setVisibility(View.GONE);
                if (task.isSuccessful()){
                    finish();
                    Intent intent = new Intent(MainActivity.this,ProfileActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }else {
                    Toast.makeText(MainActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mAuth.getCurrentUser() != null){
            finish();
            startActivity(new Intent(MainActivity.this,ProfileActivity.class));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.sign_up_text_view:
                finish();
                //intent shortcut.
                startActivity (new Intent(this,SignUpActivity.class));
                break;
            case R.id.btn_login:
                userLogin();
                break;
        }
    }
}
